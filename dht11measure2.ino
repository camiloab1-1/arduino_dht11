
//
//    FILE: dht11_test.ino
//  AUTHOR: Rob Tillaart
// VERSION: 0.1.01
// PURPOSE: DHT library test sketch for DHT11 && Arduino
//     URL:
//
// Released to the public domain
// Modified by Camilo Arzuza Bonett, March 23rd, 2015
// Last modification by Camilo Arzuza Bonett, April 21st, 2015


#include <dht.h>
#include <SoftwareSerial.h>
#include <PID_v1.h>
#include <PID_AutoTune_v0.h>

dht DHT;
SoftwareSerial BBT(12, 13); //pin 12 RX, pin 13 TX

char junk;

#define DHT11_PIN1 7
#define DHT11_PIN2 8
#define PinPWM1 5
#define PinPWM2 6
#define PinSol  3

byte ATuneModeRemember=2;
double input = 80, output=50, setpoint=180;
double kp=2,ki=0.5,kd=2;

double kpmodel=1.5, taup=100, theta[50];
double outputStart=5;
double aTuneStep=50, aTuneNoise=1, aTuneStartValue=100;
unsigned int aTuneLookBack=20;

boolean tuning = false;
unsigned long  modelTime, serialTime;

PID myPID(&input, &output, &setpoint,kp,ki,kd, DIRECT);
PID_ATune aTune(&input, &output);

double PWMOut = output*255;

boolean useSimulation = false;


void setup()
{
  BBT.begin(9600);           // set up Serial library at 9600 bps

  BBT.flush();

  BBT.println("Parametros para el ajuste del sistema");
  BBT.println("\n\n");

  
    BBT.println("Inserte un valor para el SetPoint, luego ENTER");
  while (BBT.available() == 0) ;  // Wait here until input buffer has a character
  {
      //Side 1
    setpoint = BBT.parseFloat();        // new command in 1.0 forward
    BBT.print("setpoint = "); 
    BBT.println(setpoint, DEC);
    while (BBT.available() > 0)  // .parseFloat() can leave non-numeric characters
    { junk = BBT.read() ; }      // clear the keyboard buffer
  }
  
  
  
  BBT.println("Inserte un valor de salida para el error, luego ENTER");
  while (BBT.available() == 0) ;  // Wait here until input buffer has a character
  {
      //Side 1
    output = BBT.parseFloat();        // new command in 1.0 forward
    BBT.print("output = "); BBT.println(output, DEC);

    while (BBT.available() > 0)  // .parseFloat() can leave non-numeric characters
    { junk = BBT.read() ; }      // clear the keyboard buffer
  }
  
  
  
    BBT.println("Inserte un valor para la ganancia, luego ENTER");
  while (BBT.available() == 0) ;  // Wait here until input buffer has a character
  {
      //Side 1
    kp = BBT.parseFloat();        // new command in 1.0 forward
    BBT.print("kp = "); BBT.println(kp, DEC);

    while (BBT.available() > 0)  // .parseFloat() can leave non-numeric characters
    { junk = BBT.read() ; }      // clear the keyboard buffer
  }
  
  
  
    BBT.println("Inserte un valor para la constante parte integral, luego ENTER");
  while (BBT.available() == 0) ;  // Wait here until input buffer has a character
  {
      //Side 1
    ki = BBT.parseFloat();        // new command in 1.0 forward
    BBT.print("ki = "); BBT.println(ki, DEC);

    while (BBT.available() > 0)  // .parseFloat() can leave non-numeric characters
    { junk = BBT.read() ; }      // clear the keyboard buffer
  }
  
  
  
  
    BBT.println("Inserte un valor para la ganancia parte derivativa, luego ENTER");
  while (BBT.available() == 0) ;  // Wait here until input buffer has a character
  {
      //Side 1
    kd = BBT.parseFloat();        // new command in 1.0 forward
    BBT.print("kd = "); BBT.println(kd, DEC);

    while (BBT.available() > 0)  // .parseFloat() can leave non-numeric characters
    { junk = BBT.read() ; }      // clear the keyboard buffer
  }
 


  pinMode(PinPWM1, OUTPUT);
  pinMode(PinPWM2, OUTPUT);
  pinMode(PinSol, OUTPUT);
    
  BBT.println("DHT TEST PROGRAM ");
  BBT.print("LIBRARY VERSION: ");
  BBT.println(DHT_LIB_VERSION);
  BBT.println("Type,\tstatus,\tHumidity (%),\tTemperature (C)\n");
}

void loop()
{
  // READ DATA
  BBT.print("DHT11#1:, \t");
  int chk1 = DHT.read11(DHT11_PIN1);
  
  switch (chk1)
  {
    case DHTLIB_OK:
      BBT.print("OK,\t");
      break;
    case DHTLIB_ERROR_CHECKSUM:
      BBT.print("Checksum error,\t");
      break;
    case DHTLIB_ERROR_TIMEOUT:
      BBT.print("Time out error,\t");
      break;
    case DHTLIB_ERROR_CONNECT:
      BBT.print("Connect error,\t");
      break;
    case DHTLIB_ERROR_ACK_L:
      BBT.print("Ack Low error,\t");
      break;
    case DHTLIB_ERROR_ACK_H:
      BBT.print("Ack High error,\t");
      break;
    default:
      BBT.print("Unknown error,\t");
      break;
  }

  BBT.print("DHT11#2:, \t");
  int chk2 = DHT.read11(DHT11_PIN2);

  switch (chk2)
  {
    case DHTLIB_OK:
      BBT.print("OK,\t");
      break;
    case DHTLIB_ERROR_CHECKSUM:
      BBT.print("Checksum error,\t");
      break;
    case DHTLIB_ERROR_TIMEOUT:
      BBT.print("Time out error,\t");
      break;
    case DHTLIB_ERROR_CONNECT:
      BBT.print("Connect error,\t");
      break;
    case DHTLIB_ERROR_ACK_L:
      BBT.print("Ack Low error,\t");
      break;
    case DHTLIB_ERROR_ACK_H:
      BBT.print("Ack High error,\t");
      break;
    default:
      BBT.print("Unknown error,\t");
      break;
  }
  // DISPLAY DATA
  
  BBT.print("DHT11#1:, \t");
  BBT.print(DHT.humidity, 1);
  input = BBT.parseInt(); 
  BBT.print(",\t");
  BBT.println(DHT.temperature, 1);
  BBT.print(",\n");
  
  analogWrite(PinPWM1, PWMOut);
  
  delay(1000);
  BBT.print(",\n");
  BBT.print("DHT11#2:, \t");
  BBT.print(",\t");
  BBT.print(DHT.humidity, 1);
  BBT.print(",\t");
  BBT.println(DHT.temperature, 1);
  
  analogWrite(PinPWM2, PWMOut);
  
  if(output>0.5)
  {
    digitalWrite(PinSol, LOW);
  }
  
  else if(output <= 0.5)
    {
    digitalWrite(PinSol, HIGH);
  }
  
  
   unsigned long now = millis();

  if(!useSimulation)
  { //pull the input in from the real world
    input = DHT.read11(DHT11_PIN1);
  }
  
  if(tuning)
  {
    byte val = (aTune.Runtime());
    if (val!=0)
    {
      tuning = false;
    }
    if(!tuning)
    { //we're done, set the tuning parameters
      kp = aTune.GetKp();
      ki = aTune.GetKi();
      kd = aTune.GetKd();
      myPID.SetTunings(kp,ki,kd);
      AutoTuneHelper(false);
    }
  }
  else myPID.Compute();
  
  if(useSimulation)
  {
    theta[30]=output;
    if(now>=modelTime)
    {
      modelTime +=100; 
      DoModel();
    }
  }
  else
  {
     analogWrite(0,PinPWM1); 
  }
  
  //send-receive with processing if it's time
  if(millis()>serialTime)
  {
    SerialReceive();
    SerialSend();
    serialTime+=500;
  }
  
  //analogWrite(PinPWM, 0);
}

void changeAutoTune()
{
 if(!tuning)
  {
    //Set the output to the desired starting frequency.
    output=aTuneStartValue;
    aTune.SetNoiseBand(aTuneNoise);
    aTune.SetOutputStep(aTuneStep);
    aTune.SetLookbackSec((int)aTuneLookBack);
    AutoTuneHelper(true);
    tuning = true;
  }
  else
  { //cancel autotune
    aTune.Cancel();
    tuning = false;
    AutoTuneHelper(false);
  }
}

void AutoTuneHelper(boolean start)
{
  if(start)
    ATuneModeRemember = myPID.GetMode();
  else
    myPID.SetMode(ATuneModeRemember);
}


void SerialSend()
{
  delay(1500);
  BBT.print(",\n");
  BBT.print(",\n");
  BBT.print("setpoint: ");BBT.print(setpoint); BBT.print(" ");
  BBT.print("input: ");BBT.print(input); BBT.print(" ");
  BBT.print("output: ");BBT.print(output); BBT.print(" ");
  if(tuning){
    BBT.print(",\n");
    BBT.println("tuning mode");
  } else {
    BBT.print(",\n");
    BBT.print("kp: ");BBT.print(myPID.GetKp());BBT.print(" ");
    BBT.print("ki: ");BBT.print(myPID.GetKi());BBT.print(" ");
    BBT.print("kd: ");BBT.print(myPID.GetKd());BBT.println();
  }
}

void SerialReceive()
{
  if(BBT.available())
  {
   char b = BBT.read(); 
   BBT.flush(); 
   if((b=='1' && !tuning) || (b!='1' && tuning))changeAutoTune();
  }
}

void DoModel()
{
  //cycle the dead time
  for(byte i=0;i<49;i++)
  {
    theta[i] = theta[i+1];
  }
  //compute the input
  input = ((kpmodel / taup) *(theta[0]-outputStart) + input*(1-1/taup) + ((float)random(-10,10))/100)*DHT.humidity;

}
//
// END OF FILE
//
